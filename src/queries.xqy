xquery version "3.0";

import module namespace v = "http://h2oconsulting.be/trainer/view"
   at "lib/view.xqy";

declare namespace cts  = "http://marklogic.com/cts";
declare namespace xdmp = "http://marklogic.com/xdmp";

(: TODO: Add the ability to delete user queries. :)
let $user := xdmp:get-current-user()
let $coll := '/xmlss/queries/' || $user || '/'
let $uris := cts:uri-match($coll || '*')
let $page :=
   <wrapper xmlns="http://www.w3.org/1999/xhtml">
      <h1>Queries</h1>
      <p>Here are some sample queries.  The sections correspond to the sections in the class
         <a href="http://fgeorges.org/papers/fgeorges-xmlss-xslt-trends-2015.pdf">Trends in
         XSLT and XQuery</a>, by Florent Georges, for the Oxford
         <a href="http://xmlsummerschool.com/">XML Summer School</a>.</p>
      <h2>User queries</h2>
      <p>Personal queries for user <code>{ $user }</code>.</p>
      {
         if ( fn:empty($uris) ) then
            <p>No query found.</p>
         else
            <ul> {
               $uris
                  ! fn:tokenize(., '/')[fn:last()]
                  ! <li><a href="index?user-query={ . }">{ . }</a></li>
            }
            </ul>
      }
      <h2>Convenience operators</h2>
      <ul>
         <li><a href="index?query=concat.xq">Concat operator</a></li>
         <li><a href="index?query=mapping.xq">Mapping operator</a></li>
         <li><a href="index?query=new-functions.xq">New functions</a> in the standard library</li>
      </ul>
   </wrapper>/*
return
   v:view('Queries', 'queries', $page, ())
