xquery version "3.0";

import module namespace v = "http://h2oconsulting.be/trainer/view"
   at "lib/view.xqy";

declare namespace xdmp = "http://marklogic.com/xdmp";
declare namespace cts  = "http://marklogic.com/cts";

declare variable $page :=
   <wrapper xmlns="http://www.w3.org/1999/xhtml">
      <div class="jumbotron">
         <h1>Personal Trainer</h1>
         <p>For a healthy XML life.</p>
      </div>

      <h1>Evaluator</h1>
      <p>Select one document on the left-hand side, to serve as the input of your query.</p>
      <ul> {
         for $d in cts:uri-match('/xmlss/docs/*')
         return
            <li>{ fn:substring-after($d, '/xmlss/docs/') }</li>
      }
      </ul>

      <h2>Query</h2>
      <pre id="query" style="height: 300pt">
         <code class="language-xml"> {
            local:get-query(
               xdmp:get-request-field('query'),
               xdmp:get-request-field('user-query'))
         }
         </code>
      </pre>
      <div class="form-inline">
         <button type="button" class="btn btn-primary active" onclick="evalQuery();">Evaluate</button>
         <div class="form-group">
            <input type="text" class="form-control" id="query-name" placeholder="my-query.xq"/>
         </div>
         <button type="submit" class="btn btn-default" onclick="saveQuery();">Save</button>
      </div>

{(:
      <h2>Input</h2>
      <p>Input can be accessed through <code>$in</code> in the query.</p>
      <pre id="input" style="height: 300pt">
         <code class="language-xml">&lt;hello>World!&lt;/hello>&#10;</code>
      </pre>
      <select id="input-name" class="form-control" onchange="loadInput(event);">
         <option>Hello, world!</option>
         {
            for $d in cts:uri-match('/xmlss/docs/*')
            order by $d
            return
               <option>{ fn:substring-after($d, '/xmlss/docs/') }</option>
         }
      </select>
:)()}

      <h2 class="error-area" style="display:none">Error</h2>
      <div class="error-area" style="display:none" id="error"/>

      <h2>Result</h2>
      <pre id="result" style="height: 300pt">
         <code id="result-area" class="language-xml"/>
      </pre>

   </wrapper>/*;

declare variable $scripts :=
   <wrapper xmlns="http://www.w3.org/1999/xhtml">
      <!-- TODO: Must be at the end, but the below script, using it, as well... -->
      <script src="js/ace/ace.js" type="text/javascript"/>
      <script type="text/javascript">
         // ACE editor pane setup for query
         var aceQuery = ace.edit("query");
         aceQuery.setTheme("ace/theme/pastel_on_dark");
         aceQuery.getSession().setMode("ace/mode/xml");

{(:
         // ACE editor pane setup for input
         var aceInput = ace.edit("input");
         aceInput.setTheme("ace/theme/pastel_on_dark");
         aceInput.getSession().setMode("ace/mode/xml");
:)()}

         // ACE editor pane setup for result (read-only)
         var aceResult = ace.edit("result");
         aceResult.setReadOnly(true);
         aceResult.setTheme("ace/theme/pastel_on_dark");
         aceResult.getSession().setMode("ace/mode/xml");

         // evaluate the query by sending it to ML, and display the result
         function evalQuery()
         {{
            var fd = new FormData();
            fd.append("query", getContent(aceQuery));
            $.ajax({{
               url: "eval",
               method: "POST",
               data: fd,
               dataType: "text",
               processData: false,
               contentType: false,
               success: function(data) {{
                  setContent(aceResult, data + "\n");
                  $(".error-area").hide();
               }},
               error: function(xhr, status, error) {{
                  // alert("Error: " + status + " (" + error + ")\n\nSee details below.");
                  $("#error").html($(xhr.responseText).find("dl"));
                  $(".error-area").show();
               }}}});
         }};

         // save the query by sending it to ML
         function saveQuery()
         {{
            var fd = new FormData();
            fd.append("query", getContent(aceQuery));
            fd.append("name", $("#query-name").val());
            $.ajax({{
               url: "save",
               method: "POST",
               data: fd,
               dataType: "text",
               processData: false,
               contentType: false,
               success: function(data) {{
                  alert(data);
                  $(".error-area").hide();
               }},
               error: function(xhr, status, error) {{
                  // alert("Error: " + status + " (" + error + ")\n\nSee details below.");
                  $("#error").html($(xhr.responseText).find("dl"));
                  $(".error-area").show();
               }}}});
         }};

         // set the value of the editor
         function setContent(editor, text)
         {{
            var sess = editor.getSession();
            var doc = sess.getDocument();
            doc.setValue(text);
         }};

         // get the query from the editor
         function getContent(editor)
         {{
            var sess = editor.getSession();
            var doc = sess.getDocument();
            return doc.getValue();
         }};

{(:
         // get the query from the editor
         function loadInput(event)
         {{
            var doc = event.target.value;
            if ( /\s/.test(doc) )
               setContent(aceInput, "<hello>World!</hello>");
            else
               $.ajax({{
                  url: "input?doc=" + doc, // TODO: URL-encode the doc name?
                  dataType: "text",
                  success: function(data) {{
                     setContent(aceInput, data + "\n");
                  }}}});
         }};
:)()}
      </script>
   </wrapper>/*;

(: TODO: Is there a way to retrieve the module URI?
 : Or any way to resolve a URI relatively to it?
 : Instead of declaring an explicit variable...
 :)
declare variable $queries-dir :=
(: '/Users/fgeorges/projects/ml/trainer/src/queries/'; :)
   '/home/fgeorges/ml-trainer/src/queries/';

declare function local:get-query(
   $query      as xs:string?,
   $user-query as xs:string?
) as xs:string
{
   if ( fn:exists($query) and fn:exists($user-query) ) then
      fn:error('Both query and user-query specified! (' || $query || ', ' || $user-query || ')')
   else if ( fn:exists($user-query) ) then
      let $user := xdmp:get-current-user()
      let $uri  := '/xmlss/queries/' || $user || '/' || $user-query
      return
         fn:doc($uri)
   else if ( fn:exists($query) ) then
      xdmp:filesystem-file($queries-dir || $query)
   else
      xdmp:filesystem-file($queries-dir || 'default.xq')
};

v:view('Trainer', 'index', $page, $scripts)
