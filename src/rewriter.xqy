xquery version "3.0";

declare namespace xdmp = "http://marklogic.com/xdmp";

declare variable $matches :=
   <matches>
      <error match=".xqy$" message="Not allowed to invoke modules directly"/>
      <match match="^/$"            replace="/index.xqy"/>
      <match match="^/index$"       replace="/index.xqy"/>
      <match match="^/inputs$"      replace="/inputs.xqy"/>
      <match match="^/queries$"     replace="/queries.xqy"/>
      <match match="^/eval$"        replace="/eval-query.xqy"/>
      <match match="^/save$"        replace="/save-query.xqy"/>
      <match match="^/input$"       replace="/get-input.xqy"/>
      <match match="^/(css|js)/.+$" replace="$0"/>
   </matches>;

declare function local:replace($url as xs:string, $matches as element()*)
   as xs:string
{
   if ( fn:empty($matches) ) then
      $url
   else if ( fn:matches($url, $matches[1]/@match) ) then
      if ( $matches[1] instance of element(error) ) then
         fn:error((), $matches[1]/@message || '($url: ' || $url || ')')
      else
         fn:replace($url, $matches[1]/@match, $matches[1]/@replace)
   else
      local:replace($url, fn:remove($matches, 1))
};

let $url := xdmp:get-request-url()
return
   if ( fn:contains($url, '?') ) then
      let $path  := fn:substring-before($url, '?')
      let $query := fn:substring-after($url, '?')
      return
         local:replace($path, $matches/*) || '?' || $query
   else
      local:replace($url, $matches/*)
