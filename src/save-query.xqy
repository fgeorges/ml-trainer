xquery version "3.0";

declare namespace xdmp = "http://marklogic.com/xdmp";

(: TODO: Create a function to get the value, check it exists (see the EXPath Console tools). :)
let $query := xdmp:get-request-field('query')
let $name  := xdmp:get-request-field('name')
let $user  := xdmp:get-current-user()
let $uri   := '/xmlss/queries/' || $user || '/' || $name
return (
   xdmp:document-insert(
      $uri,
      text { $query },
      (xdmp:permission('xmlss', 'read'),
       xdmp:permission('xmlss', 'update'))),
   'Query saved under: ' || $name)
