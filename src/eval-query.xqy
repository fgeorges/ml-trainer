xquery version "3.0";

declare namespace xdmp = "http://marklogic.com/xdmp";

(: TODO: User management: use simple "digest" auth.  Create users by hand in MarkLogic.
 : User the current user to retrieve and store documents...
 :
 : TODO: Expand the EXPath Console to create directories and documents when browsing
 : a database, and to edit an existing document when browsing it... (there is already
 : something somewhere, displaying several properties for a document, so just add an
 : ACE editor to it, like here!)
 :)

(: TODO: Create a function to get the value, check it exists (see the EXPath Console tools). :)
let $query := xdmp:get-request-field('query')
return
   xdmp:quote(
      xdmp:eval($query),
      <options xmlns="xdmp:quote">
         <indent-untyped>yes</indent-untyped>
      </options>)
