xquery version "3.0";

module namespace v = "http://h2oconsulting.be/trainer/view";

declare namespace h    = "http://www.w3.org/1999/xhtml";
declare namespace xdmp = "http://marklogic.com/xdmp";

(:~
 : TODO: Doc...
 :
 : To handle errors as well, pass a function item instead for $content, and
 : call it within a try/catch.  Only if needed...
 :)
declare function v:view(
   $title   as xs:string,
   $page    as xs:string,
   $content as element()+, (: element(h:*)+ :)
   $scripts as element(h:script)*
)
{
   <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
      <head>
         <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
         <meta name="viewport" content="width=device-width, initial-scale=1"/>
         <meta name="ml.time"  content="{ xdmp:elapsed-time() }"/>
         <title>{ $title }</title>
         <link href="css/bootstrap.css"          rel="stylesheet"/>
         <link href="css/bootstrap-theme.css"    rel="stylesheet"/>
         <link href="css/argus-rm-poc-theme.css" rel="stylesheet"/>
      </head>
      <body>
         <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                          data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"/>
                     <span class="icon-bar"/>
                     <span class="icon-bar"/>
                  </button>
                  <a class="navbar-brand" href="/">Trainer</a>
               </div>
               <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                     <li class="{ 'active'[$page eq 'index']   }"><a href="/">Home</a></li>
                     <li class="{ 'active'[$page eq 'queries'] }"><a href="/queries">Queries</a></li>
                     <li class="{ 'active'[$page eq 'inputs']  }"><a href="/inputs">Inputs</a></li>
                  </ul>
                  <p class="navbar-text navbar-right">User: { xdmp:get-current-user() }</p>
               </div>
            </div>
         </nav>
         <div class="container theme-showcase" role="main">
         {
            $content
         }
         </div>
         <script src="js/jquery.js"    type="text/javascript"/>
         <script src="js/bootstrap.js" type="text/javascript"/>
         {
            $scripts
         }
      </body>
   </html>
};
