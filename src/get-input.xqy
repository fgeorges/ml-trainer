xquery version "3.0";

declare namespace xdmp = "http://marklogic.com/xdmp";

(: TODO: Create a function to get the value, check it exists (see the EXPath Console tools). :)
let $doc := xdmp:get-request-field('doc')
return
   xdmp:quote(
      fn:doc('/xmlss/docs/' || $doc),
      <options xmlns="xdmp:quote">
         <indent-untyped>yes</indent-untyped>
      </options>)
