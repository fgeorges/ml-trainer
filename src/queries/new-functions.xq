xquery version "3.0";

declare variable $date := xs:date('2015-07-01');

<result>
   <res desc="round">
      <val>{ round(2.79) }</val>
   </res>
   <res desc="format date and time">
      <val>{ format-date($date, '[D1o] [MNn] [Y]') }</val>
      <val>{ format-date($date, '[D1o] [MNn] [Y]', 'fr') }</val>
   </res>
   <res desc="format number">
      <val>{ () }</val>
   </res>
   <res desc="generate ID">
      <val>{ () }</val>
   </res>
   <res desc="parse and serialize XML">
      <val>{ () }</val>
   </res>
   <res desc="regex">
      <val>{ () }</val>
   </res>
   <res desc="math">
      <val>{ () }</val>
   </res>
   <res desc="HOF utilities">
      <val>{ () }</val>
   </res>
</result>
