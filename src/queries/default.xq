xquery version "3.0";

declare variable $in external;

declare variable $doc := doc('/xmlss/docs/doc-1.xml');

declare variable $input :=
   <input>
      <hello>World!</hello>
   </input>;

<count>{ count(($doc, $input)//*) }</count>
