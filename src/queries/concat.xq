xquery version "3.0";

declare variable $you := 'Jane Doe';

<result>
   <one> {
      (: the concat operator :)
      'Hello, ' || $you || '!'
   }
   </one>
   <two> {
      (: the concat function :)
      concat('Hello, ', $you, '!')
   }
   </two>
</result>
