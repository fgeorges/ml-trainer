xquery version "3.0";

declare namespace err = "http://www.w3.org/2005/xqt-errors";

declare variable $elems :=
   <elems>
      <e>1</e>
      <e>2</e>
      <e>3</e>
      <e>4</e>
      <e>5</e>
   </elems>/*;

declare variable $messy := (
   $elems[2],
   $elems[4],
   $elems[2],
   $elems[1]);

<result>
   <res desc="path op on atomics"> {
      try {
         (: path operator is not applicable to atomic values :)
         (1, 2) / string(.)
      }
      catch err:XPTY0019 {
         <error code="{ $err:code }"> {
            $err:description
         }
         </error>
      }
   }
   </res>
   <res desc="for loop"> {
      for $n in $messy return <elem>{ string($n) }</elem>
   }
   </res>
   <res desc="path loop (selecting nodes)"> {
      $messy / .
   }
   </res>
   <res desc="path loop (producing nodes)"> {
      $messy / <elem>{ string(.) }</elem>
   }
   </res>
   <res desc="exclamation loop"> {
      $messy ! <elem>{ string(.) }</elem>
   }
   </res>
</result>
